package simplextests;

import org.junit.Test;

import matrix.MatrixOp;
import simplex.LinearProgramObjective;
import simplex.Simplex;

public class TestSimplex
{
	@Test
    public void testPanProblemWithSimplex()
    {
		//LinearProgram lp = new Simplex(LinearProgramObjective.MINIMIZE, 4, 2); 
		/*for(int i = 0; i < 4; i++) {
			lp.addVar(i, new LinearProgramVariable("x"+i, 0, Double.MAX_VALUE));
		}*/
	/*	lp.addConstraint(0, new LinearProgramConstraint("Medium Pan", 
				Arrays.asList(8.0,4.0,2.0,0.0), 500, ConstraintType.GREAT_EQUAL));
		lp.addConstraint(0, new LinearProgramConstraint("Large Pan", 
				Arrays.asList(0.0,1.0,2.0,3.0), 350, ConstraintType.GREAT_EQUAL));*/
		//LinearProgramVariable vars[] = lp.execute();
		/*Assertions.assertEquals(62.5, vars[0].getValue());
		Assertions.assertEquals(0, vars[1].getValue()); 
		Assertions.assertEquals(0, vars[2].getValue()); 
		Assertions.assertEquals(1050/9, vars[3].getValue()); */
		
	}
	
	@Test
    public void testProblemWithSimplex()
    {
		final double[] c = new double[] {1, 2, 3, 2, 0, 0};
		final double[][] A = new double[][] {{8, 4, 2, 0, -1, 0}, {0, 1, 2, 3, 0, -1}};
		final double[] b = new double[] {500,350};
		
		
		final Simplex lp = new Simplex(c, A, b, LinearProgramObjective.MINIMIZE, new int[] {4,5});
		
		lp.executeSteps();
		
		final double[] x = lp.getX(); 
		
		/*Assertions.assertEquals(62.5, vars[0].getValue());
		Assertions.assertEquals(0, vars[1].getValue()); 
		Assertions.assertEquals(0, vars[2].getValue()); 
		Assertions.assertEquals(1050/9, vars[3].getValue()); */
		
	}
	
	@Test
    public void testProblem2WithSimplex()
    {
		final double[] c = new double[] {3, 4, 0, 0, 0};
		final double[][] A = new double[][] {{1, 1, 1, 0, 0}, {2, 1, 0, 1, 0}, {1, 2, 0, 0, 1}};
		final double[] b = new double[] {10,15,18};
		
		
		final Simplex lp = new Simplex(c, A, b, LinearProgramObjective.MINIMIZE, new int[] {2,3,4});
		
		lp.executeSteps();
		
		final double[] x = lp.getX(); 
		
		MatrixOp.printVector(x, 3);
		
		/*Assertions.assertEquals(62.5, vars[0].getValue());
		Assertions.assertEquals(0, vars[1].getValue()); 
		Assertions.assertEquals(0, vars[2].getValue()); 
		Assertions.assertEquals(1050/9, vars[3].getValue()); */
		
	}
	
	@Test
    public void testMatrixOperations()
    {
		final double[][] A = {{0,2,3,2},{2,1,1,2},{1,2,2,2}};
		MatrixOp.printVector(MatrixOp.getCollumn(A, 0), 0);
		MatrixOp.printVector(MatrixOp.getCollumn(A, 1), 0);
		MatrixOp.printVector(MatrixOp.getCollumn(A, 2), 0);
		MatrixOp.printVector(MatrixOp.getCollumn(A, 3), 0);
		
		final double[][] B = MatrixOp.getMatrixFromCollumns(A, new int[] {1,3});
		MatrixOp.printMatrix(B, 1,"");
		MatrixOp.gaussElimination(B);
		MatrixOp.printMatrix(B,3,"");
	}
	
	@Test
    public void testLoop()
    {
		final int n = (int) Math.pow(10, 20);
		int k = 1;
		final long startTime= System.currentTimeMillis();
		for(int i = 0; i < n; i++)
			k++;
		final long endTime = System.currentTimeMillis();
		System.out.println(startTime);
		System.out.println(endTime);
		System.out.println(endTime-startTime);
		System.out.println(k);
	}

}
