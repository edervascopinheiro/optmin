package matrixtests;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import matrix.MatrixOp;

public class TestsMatrixOperations
{

	double delta = 1e-12;
	
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testVetorsMultiplications()
    {
        final double[] a = { 2, 3, 1, 4 };
        final double[] b = { 1, 2, 2, 1 };
        Assert.assertEquals(14.0, MatrixOp.vetorsMultiplication(a, b), this.delta);
        final double[] c = { 3, 4 };
        this.exceptionRule.expect(UnsupportedOperationException.class);
        this.exceptionRule.expectMessage("The length of vectors are differents");
        MatrixOp.vetorsMultiplication(a, c);
    }

    @Test
    public void testMatrixMultiplication()
    {
		final double[][] A = {{1,1,1,1},{2,2,2,2},{1,1,1,1},{2,2,2,2}};
		final double[] x = {2,2,2,2};
		final double[] result = MatrixOp.matrixVectorMultiplication(A, x);
        Assert.assertArrayEquals(new double[] { 8, 16, 8, 16 }, result, this.delta);
		MatrixOp.printVector(result, 3);
	}
	
	@Test
    public void testGaussEliminationSquare()
    {
		final double[][] A = {{0,2,3},{2,1,1},{1,2,1}};
		MatrixOp.gaussElimination(A);
        Assert.assertArrayEquals(new double[][] { { 2.0, 1.0, 1.0 }, { 0.0, 2.0, 3.0 }, { 0.0, 0.0, -7.0 / 4.0 } }, A);
		MatrixOp.printMatrix(A,3,"");
	}
	
	@Test
    public void testGaussEliminationMoreEquations()
    {
		final double[][] A = {{0,2,3},{2,1,1},{1,2,1}, {3,2,5}};
		MatrixOp.gaussElimination(A);
        Assert.assertArrayEquals(new double[][] { { 3.0, 2.0, 5.0 }, { 0.0, 2.0, 3.0 }, { 0.0, 0.0, -8.0 / 3.0 }, { 0.0, 0.0, 0.0 } }, A);
		MatrixOp.printMatrix(A,3,"");
	}
	
	@Test
    public void testGaussEliminationLessEquations()
    {
		final double[][] A = {{0,2,3,2},{2,1,1,2},{1,2,2,2}};
		MatrixOp.gaussElimination(A);
		MatrixOp.printMatrix(A,3,"");
	}
}
