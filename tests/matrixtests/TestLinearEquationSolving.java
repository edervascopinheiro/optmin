package matrixtests;

import org.junit.Assert;
import org.junit.Test;

import matrix.LinearEquationSolving;
import matrix.MatrixOp;

public class TestLinearEquationSolving
{

	double delta = 1e-12;
			
	@Test
    public void testSolveLinearEquationSquare()
    {
		final double[][] A = {{0,2,3},{2,1,1},{1,2,1}};
		final double[] b = {1,1,-7};
		MatrixOp.printVector(LinearEquationSolving.solveEquation(A, b),3);
	}
	
	@Test
    public void testSolveLinearEquationLessVariables()
    {
		final double[][] A = {{0,2,3},{2,1,1},{1,2,1}, {3,2,5}};
		final double[] b = {0,2,1,3};
		final double[] x = LinearEquationSolving.solveEquation(A, b);
		MatrixOp.gaussElimination(A, b);
        Assert.assertArrayEquals(new double[] { 1.0, 0.0, 0.0 }, x, this.delta);
		MatrixOp.printVector(x,6);
	}
	
	@Test
    public void testSolveLinearEquationMoreVariables()
    {
		final double[][] A = {{0,2,3,2},{2,1,1,2},{1,2,2,2}};
		final double[] b = {0,1,1};
		final double[] x = LinearEquationSolving.solveEquation(A, b);
        Assert.assertArrayEquals(new double[] { 1.0 / 3.0, 1.0, -2.0 / 3.0, 0 }, x, this.delta);
		MatrixOp.printVector(x,3);
		MatrixOp.printVector(A[0], 0);
	}

}

