package minimalflowtests;

import org.junit.Test;

import simplexgraph.Arc;
import simplexgraph.Graph;
import simplexgraph.MulticommodityMinimalFlow;

public class TestMultiCommodityFlow
{
	@Test
    public void testEPS6403()
    {
        final Arc[] arcsSharingCapProd1 = new Arc[3];
        final Arc[] arcsSharingCapProd2 = new Arc[3];
        final Graph[] graphs = new Graph[] { this.product1Graph(arcsSharingCapProd1), this.product2Graph(arcsSharingCapProd2) };
        final MulticommodityMinimalFlow mcmf = new MulticommodityMinimalFlow(graphs, new double[] { 50, 30, 20 },
            new Arc[][] { arcsSharingCapProd1, arcsSharingCapProd2 });
        mcmf.execute();
	}
	
    private Graph product1Graph(final Arc[] arcsSharingCapProd)
    {
        final Graph graph = new Graph("# Product 1", 6);

        graph.setRHS(1, 20.0);
        graph.setRHS(2, 50.0);
        graph.setRHS(3, 0.0);
        graph.setRHS(4, -15.0);
        graph.setRHS(5, -25.0);
        graph.setRHS(6, -30.0);

        graph.addArc(1, 2, 15.0, 2.0);
        graph.addArc(1, 3, 22.0, 3.0);
        graph.addArc(1, 4, 17.0, 2.0);
        graph.addArc(2, 3, 21.0, 5.0);
        arcsSharingCapProd[0] = graph.addArc(2, 5, 33.0, 1.0);
        arcsSharingCapProd[1] = graph.addArc(3, 4, 25.0, 3.0);
        arcsSharingCapProd[2] = graph.addArc(3, 5, 11.0, 4.0);
        graph.addArc(4, 5, 12.0, 2.0);
        graph.addArc(4, 6, 33.0, 4.0);
        graph.addArc(5, 6, 12.0, 3.0);
        return graph;
	}
	
    private Graph product2Graph(final Arc[] arcsSharingCapProd)
    {
        final Graph graph = new Graph("# Product 2", 6);

        graph.setRHS(1, 40.0);
        graph.setRHS(2, 35.0);
        graph.setRHS(3, -25.0);
        graph.setRHS(4, 0.0);
        graph.setRHS(5, -25.0);
        graph.setRHS(6, -25.0);

        graph.addArc(1, 2, 17.0, 3.0);
        graph.addArc(1, 3, 28.0, 2.0);
        graph.addArc(1, 4, 15.0, 4.0);
        graph.addArc(2, 3, 17.0, 7.0);
        arcsSharingCapProd[0] = graph.addArc(2, 5, 30.0, 1.0);
        arcsSharingCapProd[1] = graph.addArc(3, 4, 17.0, 3.0);
        arcsSharingCapProd[2] = graph.addArc(3, 5, 14.0, 5.0);
        graph.addArc(4, 5, 15.0, 7.0);
        graph.addArc(4, 6, 26.0, 2.0);
        graph.addArc(5, 6, 16.0, 1.0);

        return graph;
    }

}
