package minimalflowtests;

import java.util.HashMap;
import java.util.Map;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.glp_arc;
import org.gnu.glpk.glp_vertex;
import org.junit.Assert;
import org.junit.Test;

import simplexgraph.Graph;
import simplexgraph.MinimalCostFlow;

public class TestMinimalFlowCost
{
    @Test
    public void testProduct1() {
        final Graph graph = new Graph("# Product 1", 6);

        graph.setRHS(1, 20.0);
        graph.setRHS(2, 50.0);
        graph.setRHS(3, 0.0);
        graph.setRHS(4, -15.0);
        graph.setRHS(5, -25.0);
        graph.setRHS(6, -30.0);

        graph.addArc(1, 2, 15.0, 2.0);
        graph.addArc(1, 3, 22.0, 3.0);
        graph.addArc(1, 4, 17.0, 2.0);
        graph.addArc(2, 3, 21.0, 5.0);
        graph.addArc(2, 5, 33.0, 9.44);
        graph.addArc(3, 4, 25.0, 2.56);
        graph.addArc(3, 5, 11.0, 4.0);
        graph.addArc(4, 5, 12.0, 2.0);
        graph.addArc(4, 6, 33.0, 4.0);
        graph.addArc(5, 6, 12.0, 3.0);
        
        final Map<String, Integer> solution = new HashMap<String, Integer>(10)
        {
            private static final long serialVersionUID = 1L;

            {
                this.put("12", Integer.valueOf(0));
                this.put("13", Integer.valueOf(3));
                this.put("14", Integer.valueOf(17));
                this.put("23", Integer.valueOf(21));
                this.put("25", Integer.valueOf(29));
                this.put("34", Integer.valueOf(24));
                this.put("35", Integer.valueOf(0));
                this.put("45", Integer.valueOf(0));
                this.put("46", Integer.valueOf(26));
                this.put("56", Integer.valueOf(4));
            }
        };

        final MinimalCostFlow mcf = new MinimalCostFlow();
        mcf.integerArcCosts(graph.getGraph());
        mcf.solveMinimalCostFlow(graph.getGraph());
        for (int i = 1; i < graph.getGraph().getNv(); i++)
        {
            final glp_vertex v = GLPK.glp_java_vertex_get(graph.getGraph(), i);
            for (glp_arc a = v.getOut(); a != null; a = a.getT_next()) {
                final glp_vertex w = a.getHead();
                final double flow = GLPK.glp_java_arc_get_data(a).getX();
                System.out.printf("arc %d->%d: x = %2g; rc = %2g; ct = %2g; cap = %2g\n",
                v.getI(), w.getI(),
                    flow,
                GLPK.glp_java_arc_get_data(a).getRc(),
                GLPK.glp_java_arc_get_data(a).getCost(),
                GLPK.glp_java_arc_get_data(a).getCap());
                Assert.assertEquals(solution.get(new StringBuilder().append(a.getTail().getI()).append(a.getHead().getI()).toString()).doubleValue(),
                    flow,
                    1e-6);
            }
        }
    }
}
