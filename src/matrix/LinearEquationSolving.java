package matrix;

import javax.naming.OperationNotSupportedException;

public class LinearEquationSolving
{
	public static double[] solveEquation(double[][] A, double[] b) {
		//TODO CHECK POSSIBILITIES
		if(A.length != b.length)
			throw new UnsupportedOperationException("Number of rows in A must be equal the number of rows of b");
		int m = A.length; int n = A[0].length; int squareIndex = Math.min(m, n);
		double[] x = new double[n];
		double[][] E = LinearEquationSolving.clone(A);
		double[] e = LinearEquationSolving.clone(b);
		MatrixOp.gaussElimination(E, e);
		for(int i = squareIndex-1;i > -1; i--) {
			double backtrackSum = 0;
			for(int j = i+1; j < E[0].length; j++) {
				backtrackSum += x[j]*E[i][j]; 
			}
			x[i] = (e[i] - backtrackSum)/E[i][i];
		}
		return x;
	}
	
	private static double[][] clone(double[][] A) {
		double[][] cloned = new double[A.length][A[0].length];
		for(int i = 0; i < A.length; i++)
			for(int j = 0; j < A[0].length; j++)
				cloned[i][j] = A[i][j];
		return cloned;		
	}
	
	private static double[] clone(double[] b) {
		double[] cloned = new double[b.length];
		for(int i = 0; i < b.length; i++)
			cloned[i] = b[i];
		return cloned;		
	}
}
