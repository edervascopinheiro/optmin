package matrix;

import java.util.List;

public class MatrixOp
{
	public static double[] getCollumn(final double[][] A, final int collumnIndex) {
		final double[] collumn = new double[A.length];
		for(int i = 0; i < A.length; i++)
			collumn[i] = A[i][collumnIndex];
		return collumn;
	}
	
	public static double[][] getMatrixFromCollumns(final double[][] A, final int[] collumnIndexes) {
		final double[][] matrixFromCollumns = new double[A.length][collumnIndexes.length];
		for(int j = 0; j < collumnIndexes.length; j++)
			for(int i = 0; i < A.length; i++)
				matrixFromCollumns[i][j] = A[i][collumnIndexes[j]];
		return matrixFromCollumns;
	}
	
	public static double[] getVetorFromIndexes(final double[] vector, final int[] indexes) {
		final double[] vectorFromIndexes = new double[indexes.length];
		for(int i = 0; i < indexes.length; i++)
			vectorFromIndexes[i] = vector[indexes[i]];
		return vectorFromIndexes;
	}
	
    public static double[] getVetorFromBeteewIndexes(final double[] vector, final int from, final int to)
    {
        if (from > to)
            throw new UnsupportedOperationException("The index from must not be greater than to");
        final double[] vectorFromIndexes = new double[to - from + 1];
        for (int i = from; i <= to; i++)
            vectorFromIndexes[i-from] = vector[i];
        return vectorFromIndexes;
    }

	public static double[][] transposeMatrix(final double[][] A){
		final double[][] transposeA = new double[A[0].length][A.length];
		for(int i = 0; i < A.length;i++)
			for(int j = 0; j < A[0].length;j++)
				transposeA[j][i] = A[i][j];
		return transposeA;
	}
	
    /**
     * a - b
     */
	public static double[] subtractVectors(final double[] a, final double[] b) {
		if(a.length != b.length)
			throw new UnsupportedOperationException("Vectors dimentions are differents");
		final double[] result = new double[a.length]; 
		for(int i = 0; i < a.length; i++) {
			result[i] = a[i] - b[i];
		}
		return result;
	}
	
    public static double[] multVector(final double value, final double[] vector)
    {
        final double[] result = new double[vector.length];
        for (int i = 0; i < vector.length; i++)
            result[i] = value * vector[i];
        return result;
    }

	public static double[] matrixVectorMultiplication(final double[][] A, final double[] x) {
		if(A[0].length != x.length)
			throw new UnsupportedOperationException("Number of matrix collumn and vector rows are differents");
		final double[] result = new double[A.length];
		for(int i = 0; i < A.length; i++) {
			for(int j = 0; j < A[0].length; j++) {
				result[i] += A[i][j]*x[j];
			}
		}
		return result;
	}
	
	public static double vetorsMultiplication(final double[] a, final double[] b) {
		if(a.length != b.length)
			throw new UnsupportedOperationException("The length of vectors are differents");
		double result = 0;
		for(int i = 0; i < a.length; i++) {
			result += a[i]*b[i];
		}
		return result;
	}
	
	public static double[][] matrixMultiplication(final double[][] A, final double[][] B) {
		final int n = A[0].length;
		final int m = B.length;
		if(n != m)
			throw new UnsupportedOperationException("This Matrix can't be multiplied");
		final double[][] R = new double[A.length][B[0].length];
		for(int i = 0; i < A.length; i++) {
			for(int j = 0; j < B[0].length; j++) {
				double scalarProduct = 0;
				for(int k = 0; k < n; k++)
					scalarProduct += A[i][k]*B[k][j];
				R[i][j] = scalarProduct;
			}
			
		}
		return R;
	}
	
    //TODO
	public static List<double[][]> luFactoration(final double[][] A){
		
		return null;
	}
	
	/*
	 * Proceed line operations in the matrix A an vector b.
	 */
	public static void gaussElimination(final double[][] A) {
		final int m = A.length; final int n = A[0].length; final int squareIndex = Math.min(m, n);
		final double[][] P = MatrixOp.getIdentityMatrix(m);
		int pivoIndex;
		double pivot;
		for(int i = 0; i < squareIndex; i++) {
			pivoIndex = MatrixOp.getBestPivoIndex(A, i);
			MatrixOp.swapMatrixLines(A,pivoIndex,i);
			for(int k = i + 1; k < m; k++) {
				pivot = -A[k][i]/A[i][i];//TODO check if pivot value is zero
				if(pivot != 0) 
					for(int j = i; j < n; j++)
						A[k][j] = pivot * A[i][j]+A[k][j];
			}
		}
	}

	/*
	 * Proceed line operations in the matrix A an vector b.
	 * Then for Ax=b
	 */
	public static void gaussElimination(final double[][] A, final double[] b) {
		final int m = A.length; final int n = A[0].length; final int squareIndex = Math.min(m, n);
		int pivoIndex;
		double pivot;
		for(int i = 0; i < squareIndex; i++) {
			pivoIndex = MatrixOp.getBestPivoIndex(A, i);
			MatrixOp.swapLines(A,b,pivoIndex,i);
			for(int k = i + 1; k < m; k++) {
				pivot = -A[k][i]/A[i][i];//TODO check if pivot value is zero
				if(pivot != 0) {
					for(int j = i; j < n; j++)
						A[k][j] = pivot * A[i][j]+A[k][j];
					b[k] = pivot*b[i] + b[k];
				}
			}
		}
	}
	
	private static void swapLines(final double[][] A, final double[] b, final int row1, final int row2) {
		if(row1 != row2 && A.length > row1 && A.length > row2) {
			MatrixOp.swapMatrixLines(A, row1, row2);
			MatrixOp.swapVectorLines(b, row1, row2);
		}
	}

	private static void swapMatrixLines(final double[][] A, final int row1, final int row2) {
		if(A.length <= row1 || A.length <= row2)
			throw new UnsupportedOperationException("Impossible to swap matrix rows");
		final double[] l = A[row1];
		A[row1] = A[row2];
		A[row2] = l;
	}
	
	private static void swapVectorLines(final double[] b, final int row1, final int row2) {
		if(b.length <= row1 || b.length <= row2)
			throw new UnsupportedOperationException("Impossible to swap vector rows");
		final double l = b[row1];
		b[row1] = b[row2];
		b[row2] = l;
	}
	
	public static double[][] getIdentityMatrix(final int size){
		final double[][] P = new double[size][size];
		for(int i = 0; i < size; i++)
			for(int j=0; j<size; j++)
				P[i][j] = i == j ? 1 : 0;
		return P;
	}
	
	private static int getBestPivoIndex(final double[][] A, final int collumnIndex) {
		int bestPivoIndex = Math.min(collumnIndex, A.length-1);
		double pivo = A[bestPivoIndex][collumnIndex];
		for(int index = collumnIndex+1; index < A.length; index++) {
			if (Math.abs(pivo) < Math.abs(A[index][collumnIndex])){
				pivo = A[index][collumnIndex];
				bestPivoIndex = index;
			}
		}
		return bestPivoIndex;
	}
	
	public static void printMatrix(final double[][] A, final int decimals, final String separator) {
		for(int i = 0; i < A.length; i++)
			for(int j=0; j<A[0].length; j++) {
				System.out.print((A[i][j] < 0 ? " " : "  ") + MatrixOp.roundDouble(A[i][j],decimals)+ separator);
				if(j == A[0].length-1)
					System.out.println();
			}
		System.out.println();
	}

	public static void printVector(final double[] vector, final int decimals) {
		for(int i = 0; i < vector.length; i++)
			System.out.println((vector[i] < 0 ? " " : "  ") + MatrixOp.roundDouble(vector[i],decimals));
		System.out.println();
	}
	
	public static void printVector(final int[] vector) {
		for(int i = 0; i < vector.length; i++)
			System.out.println((vector[i] < 0 ? " " : "  ") + vector[i]);
		System.out.println();
	}
	
	public static double roundDouble(final double value, final int decimals) {
		final double factor = Math.pow(10,decimals);
		return Math.round(value*factor)/factor;
	}
	
	
	
	public static double[][] invert(final double A[][]) 
    {	
        final int n = A.length;
        final double[][] a = new double[n][n];
        for(int i = 0; i < n; i++)
        	for(int j = 0; j < n; j++)
        		a[i][j] = A[i][j];
        
        final double x[][] = new double[n][n];
        final double b[][] = new double[n][n];
        final int index[] = new int[n];
        for (int i=0; i<n; ++i) 
            b[i][i] = 1;
 
        // Transform the matrix into an upper triangle
        MatrixOp.gaussian(a, index);
 
        // Update the matrix b[i][j] with the ratios stored
        for (int i=0; i<n-1; ++i)
            for (int j=i+1; j<n; ++j)
                for (int k=0; k<n; ++k)
                    b[index[j]][k]
                    	    -= a[index[j]][i]*b[index[i]][k];
 
        // Perform backward substitutions
        for (int i=0; i<n; ++i) 
        {
            x[n-1][i] = b[index[n-1]][i]/a[index[n-1]][n-1];
            for (int j=n-2; j>=0; --j) 
            {
                x[j][i] = b[index[j]][i];
                for (int k=j+1; k<n; ++k) 
                {
                    x[j][i] -= a[index[j]][k]*x[k][i];
                }
                x[j][i] /= a[index[j]][j];
            }
        }
        return x;
    }
 
	// Method to carry out the partial-pivoting Gaussian
	// elimination.  Here index[] stores pivoting order.
 
    public static void gaussian(final double a[][], final int index[]) 
    {
        final int n = index.length;
        final double c[] = new double[n];
 
        // Initialize the index
        for (int i=0; i<n; ++i) 
            index[i] = i;
 
        // Find the rescaling factors, one from each row
        for (int i=0; i<n; ++i) 
        {
            double c1 = 0;
            for (int j=0; j<n; ++j) 
            {
                final double c0 = Math.abs(a[i][j]);
                if (c0 > c1) c1 = c0;
            }
            c[i] = c1;
        }
 
        // Search the pivoting element from each column
        int k = 0;
        for (int j=0; j<n-1; ++j) 
        {
            double pi1 = 0;
            for (int i=j; i<n; ++i) 
            {
                double pi0 = Math.abs(a[index[i]][j]);
                pi0 /= c[index[i]];
                if (pi0 > pi1) 
                {
                    pi1 = pi0;
                    k = i;
                }
            }
 
            // Interchange rows according to the pivoting order
            final int itmp = index[j];
            index[j] = index[k];
            index[k] = itmp;
            for (int i=j+1; i<n; ++i) 	
            {
                final double pj = a[index[i]][j]/a[index[j]][j];
 
                // Record pivoting ratios below the diagonal
                a[index[i]][j] = pj;
 
                // Modify other elements accordingly
                for (int l=j+1; l<n; ++l)
                    a[index[i]][l] -= pj*a[index[j]][l];
            }
        }
    }
    
	public static double[][] inverse(final double[][] matrix) {
		final double[][] inverse = new double[matrix.length][matrix.length];

		// minors and cofactors
		for (int i = 0; i < matrix.length; i++)
			for (int j = 0; j < matrix[i].length; j++)
				inverse[i][j] = Math.pow(-1, i + j)
						* MatrixOp.determinant(MatrixOp.minor(matrix, i, j));

		// adjugate and determinant
		final double det = 1.0 / MatrixOp.determinant(matrix);
		for (int i = 0; i < inverse.length; i++) {
			for (int j = 0; j <= i; j++) {
				final double temp = inverse[i][j];
				inverse[i][j] = inverse[j][i] * det;
				inverse[j][i] = temp * det;
			}
		}

		return inverse;
	}
	
	private static double determinant(final double[][] matrix) {
		if (matrix.length != matrix[0].length)
			throw new IllegalStateException("invalid dimensions");

		if (matrix.length == 2)
			return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];

		double det = 0;
		for (int i = 0; i < matrix[0].length; i++)
			det += Math.pow(-1, i) * matrix[0][i]
					* MatrixOp.determinant(MatrixOp.minor(matrix, 0, i));
		return det;
	}

	private static double[][] minor(final double[][] matrix, final int row, final int column) {
		final double[][] minor = new double[matrix.length - 1][matrix.length - 1];

		for (int i = 0; i < matrix.length; i++)
			for (int j = 0; i != row && j < matrix[i].length; j++)
				if (j != column)
					minor[i < row ? i : i - 1][j < column ? j : j - 1] = matrix[i][j];
		return minor;
	}
}
