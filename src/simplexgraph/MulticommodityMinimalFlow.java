package simplexgraph;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import org.gnu.glpk.glp_graph;

import matrix.MatrixOp;

public class MulticommodityMinimalFlow
{
    private final Graph[] graphs;

    private final Arc[][] arcsSharingCap;

    private final double[] rhs; //right hand side vector

    private final double[][] RSM; //revised simplex matrix
    
    private final double[][] basicCollumn;

    private final int[] basicCollumnGraphIndex;

    private final MinimalCostFlow mcf;

    private int indexNewBasicCollumn;

    private final double bigM = 1000;
    
    public MulticommodityMinimalFlow(final Graph[] graphs, final double[] u, final Arc[][] commonCapArcs)
    {
    	this.graphs = graphs;
    	this.arcsSharingCap = commonCapArcs;
    	final int rowsNumber = this.arcsSharingCap[0].length + this.graphs.length;
    	this.rhs = new double[u.length + this.arcsSharingCap[0].length];
        this.rhs[0] = 0;
        for (int i = 1; i < this.rhs.length; i++)
        	this.rhs[i] = i < u.length+1 ? u[i - 1] : 1;        
        this.mcf = new MinimalCostFlow();
        for (final Graph graph : graphs)
            this.mcf.integerArcCosts(graph.getGraph());
        final int collumnsNumbers = this.arcsSharingCap[0].length + this.graphs.length;
        this.RSM = new double[rowsNumber + 1][collumnsNumbers];
        this.basicCollumn = new double[collumnsNumbers][this.graphs[0].getArcs().size()];
        this.basicCollumnGraphIndex = new int[collumnsNumbers];
    }

    private void firstRSMInverse()
    {
        final int rowsNumber = this.RSM.length;
        final int collumnsNumbers = this.RSM[0].length;
        
        final double[][] B = new double[rowsNumber-1][collumnsNumbers];

        final double[] cb = new double[collumnsNumbers];
        
        for(int k = 0; k < this.graphs.length; k++)
        {
	        cb[k] = this.mcf.solveMinimalCostFlow(this.graphs[k].getGraph());
            final double[] collumn = MatrixOp.getVetorFromBeteewIndexes(this.getCollumn(cb[k], k), 1, rowsNumber - 1);
            this.indexNewBasicCollumn = k;
            this.saveBasicCollumn(k);
	        for (int i = 0; i < this.arcsSharingCap[k].length; i++)
	            B[i][k] = collumn[i];
	        B[rowsNumber - 3 + k][k] = 1;
        }

        for (int j = 2; j < collumnsNumbers; j++)
        {
            cb[j] = 0;
            for (int i = 0; i < this.arcsSharingCap[0].length; i++)
            	B[i][j] = i+2 == j ? 1 : 0;
        }
        
        final double[][] invB = MatrixOp.inverse(B);
        
        final double[] dualVector = MatrixOp.matrixVectorMultiplication(MatrixOp.transposeMatrix(invB), cb); 
        for(int j = 0; j < collumnsNumbers;j++)
        	this.RSM[0][j] = dualVector[j];
       
        for(int i = 1; i < rowsNumber; i++)
        	for(int j = 0; j < collumnsNumbers; j++)
        		this.RSM[i][j] = invB[i-1][j];    
        
        final double[] bbar = MatrixOp.matrixVectorMultiplication(invB,
        		MatrixOp.getVetorFromBeteewIndexes(this.rhs, 1, this.rhs.length-1));        
        for(int i = 1; i < rowsNumber; i++)
        	this.rhs[i] = bbar[i-1];
        this.rhs[0] = MatrixOp.vetorsMultiplication(cb, bbar);
    }


    private void saveBasicCollumn(final int collumnToRemove)
    {
    	final double[] collumnFlow = new double[this.graphs[0].getArcs().size()];
        if(this.indexNewBasicCollumn >= 0)
        {
	    	this.basicCollumnGraphIndex[collumnToRemove] = this.indexNewBasicCollumn + 1;
	        final List<Arc> arcs = this.graphs[this.indexNewBasicCollumn].getArcs();
	        for (int i = 0; i < arcs.size(); i++)
	            collumnFlow[i] = arcs.get(i).getFlow();
        }
        this.basicCollumn[collumnToRemove] = collumnFlow;
    }

    public void execute()
    {
    	this.firstRSMInverse();
        boolean isOptimal = this.checkFeasibilityForOptimality();
        this.printFirstBase();
        int stepCounter = 1;
        while (!isOptimal)
            isOptimal = this.executeOneStep(stepCounter++);
        if (!isOptimal)
            System.out.println("There is not an optimal solution");
        this.printOptimalSolutions();
    }

	private boolean checkFeasibilityForOptimality()
    {
        double value;
        for (int i = 1; i < this.rhs.length; i++)
        {
            value = this.rhs[i];
            if (value < 0)
            {
                this.artificialVarForBigM(i-1);
                return false;
            }
        }
        return true;
    }

	//TODO melhorar
    private void artificialVarForBigM(final int slackVariableIndex)
    {
        final double[] collumn = new double[this.RSM.length];
        for (int i = 1; i < this.RSM.length; i++)
            collumn[i] = slackVariableIndex == i+1 ? -1 : 0;
        double[] a = MatrixOp.getVetorFromBeteewIndexes(collumn, 1, collumn.length-1);
        collumn[0] = MatrixOp.vetorsMultiplication(this.RSM[0], a) - this.bigM;
        a = MatrixOp.matrixVectorMultiplication(this.RSM, a);
        for (int i = 1; i < this.RSM.length; i++)
            collumn[i] = a[i];
        this.pivot(collumn, slackVariableIndex+1);
    }

    private boolean executeOneStep(int stepCounter)
    {
        final double[] newCollumn = this.pricingStep();
        if (newCollumn == null)
            return true;
        final int outputCollumn = this.pivotation(newCollumn);
        this.saveBasicCollumn(outputCollumn);
        this.printStep(stepCounter, newCollumn, outputCollumn);
        return false;
    }

	private double[] pricingStep()
    {
        final double[] dualVar = this.dualVariables();
        final double[] gama = MatrixOp.getVetorFromBeteewIndexes(dualVar, 0, this.arcsSharingCap.length);
        final double[] alpha = MatrixOp.getVetorFromBeteewIndexes(dualVar, this.arcsSharingCap.length + 1, dualVar.length - 1);
        final double[] newCollumn = this.chooseNewCollumn(gama, alpha);
        return newCollumn;
    }

    private double[] dualVariables()
    {
        return this.RSM[0];
    }

    private double[] chooseNewCollumn(final double[] gama, final double[] alpha)
    {
    	for(int i = 0; i < gama.length; i++)
    	{
    		if(gama[i] > 0)
    		{
    			final double[] collumn = new double[this.RSM.length];
    			this.indexNewBasicCollumn = -1;
    			collumn[i+1] = 1;
    			return collumn;
    		}
    	}
    		
        double solutionTotalCost;
        glp_graph graph; 
        for (int i = 0; i < this.graphs.length; i++)
        {
            graph = this.graphs[i].getGraph();
            this.updateGraphArcCost(gama, i);
            solutionTotalCost = this.mcf.solveMinimalCostFlow(graph);
            //this.mcf.printSolution(graph);
            //System.out.println();
            if (solutionTotalCost < alpha[i])
            {
                this.indexNewBasicCollumn = i;
                return this.getCollumn(-solutionTotalCost + alpha[i], i);
            }
        }
        return null;
    }

    public void updateGraphArcCost(final double[] gama, final int indexGraph)
    {
        //System.out.println("before");
        //this.graphs[indexGraph].printGraph();
        for (int i = 0; i < this.arcsSharingCap[indexGraph].length; i++)
        {
            final Arc arc = this.arcsSharingCap[indexGraph][i];
            this.mcf.setArcCostInterging(arc.getGlpArc(), arc.getRealCost() - gama[i]);
        }
        //System.out.println("after");
       //this.graphs[indexGraph].printGraph();
    }

    private double[] getCollumn(final double cost, final int graphIndex)
    {
        final double[] collumn = new double[this.RSM.length];
        collumn[0] = cost;
        for (int i = 0; i < this.arcsSharingCap[graphIndex].length; i++)
            collumn[i+1] = this.arcsSharingCap[graphIndex][i].getFlow();
        collumn[this.arcsSharingCap.length+graphIndex+2] = 1.0;
        return collumn;
    }


    private int pivotation(final double[] newCollumn)
    {
        final double[] pivotCollumn = MatrixOp.matrixVectorMultiplication(this.RSM,
            MatrixOp.getVetorFromBeteewIndexes(newCollumn, 1, newCollumn.length - 1));
        pivotCollumn[0] = newCollumn[0];
        final int indexOutputVariable = this.radioTest(pivotCollumn);
        this.pivot(pivotCollumn, indexOutputVariable);
        return indexOutputVariable - 1;
    }
    
    private int radioTest(final double[] BNj)
    {
        double min = Double.MAX_VALUE;
        double quocient;
        int indexOutputVariable = -1;//TODO improve it
        for (int k = 1; k < this.rhs.length; k++)
        {
            if (BNj[k] > 0)
            {
                quocient = this.rhs[k] / BNj[k];
                if (min > quocient)
                {
                    min = quocient;
                    indexOutputVariable = k;
                }
            }
        }
        return indexOutputVariable;
    }
    
    /*
     * Pivot the matrix B over newCollum and the row number indexOutputVariable
     */
    private void pivot(final double[] pivotCollumn, final int indexOutputVariable)
    {
        final int n = this.RSM[0].length;
        for (int j = 0; j < n; j++)
            this.pivotRSMCollumn(j, pivotCollumn, indexOutputVariable);
        this.pivotRHS(pivotCollumn, indexOutputVariable);
    }


	private void pivotRSMCollumn(final int indexRSMCollumn, final double[] newCollumn, final int indexOutputVariable)
    {
        final double denominator = newCollumn[indexOutputVariable];
        final double pivot = this.RSM[indexOutputVariable][indexRSMCollumn]/ denominator;
        if (denominator == 0)
        	throw new UnsupportedOperationException("The pivot value must be different from zero");
        for (int i = 0; i < this.RSM.length; i++)
            this.RSM[i][indexRSMCollumn] = i == indexOutputVariable ? this.RSM[i][indexRSMCollumn]/denominator 
            		: this.RSM[i][indexRSMCollumn]  - pivot * newCollumn[i];
    }
	
	private void pivotRHS(final double[] newCollumn, final int indexOutputVariable) 
	{
		final double denominator = newCollumn[indexOutputVariable];
        final double pivot = this.rhs[indexOutputVariable]/ denominator;
        if (pivot == 0)
        	throw new UnsupportedOperationException("The pivot value must be different from zero");
        for (int i = 0; i < this.rhs.length; i++)
        	this.rhs[i] = i == indexOutputVariable ? pivot : this.rhs[i]  - pivot * newCollumn[i];
	}

    private void printOptimalSolutions() {
        System.out.println("Solução Ótima: Custo = "+MatrixOp.roundDouble(this.rhs[0],2));
        final double[][] solution = new double[this.graphs.length][this.graphs[0].getArcs().size()];
        int indexGraph;
        for (int i = 0; i < this.basicCollumnGraphIndex.length; i++)
    	{
            indexGraph = this.basicCollumnGraphIndex[i];
            if (indexGraph != 0)
                solution[indexGraph - 1] = MatrixOp.subtractVectors(solution[indexGraph - 1],
                    MatrixOp.multVector(-this.rhs[i + 1], this.basicCollumn[i]));
    	}
        System.out.println("Arc:; " + "Prod 1;" + "Prod 2; " + "Total");
        Arc arc, arc2;
        for (int i = 0; i < this.graphs[0].getArcs().size(); i++)
        {
            arc = this.graphs[0].getArcs().get(i);
            arc2 = this.graphs[1].getArcs().get(i);
            if (arc.getGlpArc().getTail().getI() != arc2.getGlpArc().getTail().getI()
                && arc.getGlpArc().getHead().getI() != arc.getGlpArc().getHead().getI())
                System.out.println("Different");

            System.out
                .printf("%d->%d:;%5.2f;%5.2f;%5.2f\n", arc.getGlpArc().getTail().getI(), arc.getGlpArc().getHead().getI(), solution[0][i],
                    solution[1][i],
                    solution[0][i] + solution[1][i]);
        }
	}
    
    private void printStep(int stepCounter, double[] newCollumn, int outputCollumn) 
    {
    	System.out.println("Passo"+stepCounter);
    	final NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
    	nf.setMinimumFractionDigits(2);
    	System.out.println("Entra; Sai");
    	System.out.printf("%s; %s\n", nf.format(newCollumn[0]), "Coluna " + (outputCollumn+1));
    	for(int i = 1; i < newCollumn.length; i++)
    		System.out.println(nf.format(newCollumn[i]));
    	System.out.println();
	}
    
    private void printFirstBase() 
    {
    	System.out.println("Passo0");
		double[][] B = new double[this.RSM.length-1][this.RSM[0].length];
		for(int i = 1; i < this.RSM.length; i++)
			for(int j = 0; j < this.RSM[0].length;j++)
				B[i-1][j] = this.RSM[i][j];
		B = MatrixOp.inverse(B);
		MatrixOp.printMatrix(B, 2, ";");
	}
}
