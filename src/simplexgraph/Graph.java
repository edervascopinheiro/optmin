package simplexgraph;

import java.util.ArrayList;
import java.util.List;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
import org.gnu.glpk.glp_arc;
import org.gnu.glpk.glp_graph;
import org.gnu.glpk.glp_java_arc_data;
import org.gnu.glpk.glp_vertex;

public class Graph
{
    private final glp_graph graph;

    private final List<Arc> arcs;

    public Graph(final String name, final int verticesQtd)
    {
        this.graph = GLPK.glp_create_graph(GLPKConstants.GLP_JAVA_V_SIZE, GLPKConstants.GLP_JAVA_A_SIZE);
        this.arcs = new ArrayList<Arc>();
        GLPK.glp_set_graph_name(this.graph, name);
        GLPK.glp_add_vertices(this.graph, 6);
        for (int i = 1; i <= verticesQtd; i++)
            GLPK.glp_set_vertex_name(this.graph, i, "v" + i);
    }

    public Arc addArc(final int from, final int to, final double cap, final double cost)
    {
        final glp_arc arc = GLPK.glp_add_arc(this.graph, from, to);
        glp_java_arc_data adata;
        adata = GLPK.glp_java_arc_get_data(arc);
        adata.setLow(0.0);
        adata.setCap(cap);
        adata.setCost(cost);
        final Arc theArc = new Arc(arc);
        this.arcs.add(theArc);
        return theArc;
    }

    public glp_graph getGraph()
    {
        return this.graph;
    }

    public void setRHS(final int vertexIndex, final double value)
    {
        GLPK.glp_java_vertex_data_get(this.graph, vertexIndex).setRhs(value);
    }

    public List<Arc> getArcs()
    {
        return this.arcs;
    }

    public void printGraph()
    {
        for (int i = 1; i < this.graph.getNv(); i++)
        {
            final glp_vertex v = GLPK.glp_java_vertex_get(this.graph, i);
            for (glp_arc a = v.getOut(); a != null; a = a.getT_next())
            {
                final glp_vertex w = a.getHead();
                final double flow = GLPK.glp_java_arc_get_data(a).getX();
                System.out.printf("arc %d->%d: x = %2g; rc = %2g; ct = %2g; cap = %2g\n", v.getI(), w.getI(), flow,
                    GLPK.glp_java_arc_get_data(a).getRc(), GLPK.glp_java_arc_get_data(a).getCost(), GLPK.glp_java_arc_get_data(a).getCap());
            }
        }
    }
}
