package simplexgraph;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.glp_arc;

public class Arc
{
    private final glp_arc glpArc;

    private final double realCost;

    public Arc(final glp_arc arc)
    {
        this.glpArc = arc;
        this.realCost = GLPK.glp_java_arc_get_data(arc).getCost();
    }

    public double getRealCost()
    {
        return this.realCost;
    }

    public glp_arc getGlpArc()
    {
        return this.glpArc;
    }

    public double getFlow()
    {
        return GLPK.glp_java_arc_get_data(this.glpArc).getX();
    }

}
