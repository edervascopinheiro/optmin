package simplexgraph;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
import org.gnu.glpk.GlpkException;
import org.gnu.glpk.SWIGTYPE_p_double;
import org.gnu.glpk.glp_arc;
import org.gnu.glpk.glp_graph;
import org.gnu.glpk.glp_java_arc_data;
import org.gnu.glpk.glp_vertex;

public class MinimalCostFlow
{
    final double mult = 100000;

    public double solveMinimalCostFlow(final glp_graph graph)
    {
        final SWIGTYPE_p_double costArray = GLPK.new_doubleArray(1);
        final int crash = 1; // ou 1
        final int ret = GLPK.glp_mincost_relax4(graph, GLPKConstants.GLP_JAVA_V_RHS, GLPKConstants.GLP_JAVA_A_LOW, GLPKConstants.GLP_JAVA_A_CAP,
            GLPKConstants.GLP_JAVA_A_COST, crash, costArray, GLPKConstants.GLP_JAVA_A_X, GLPKConstants.GLP_JAVA_A_RC);
        if (ret != 0)
            throw new GlpkException("Returned " + ret);
        final double solutionCost = GLPK.doubleArray_getitem(costArray, 0);
        GLPK.delete_doubleArray(costArray);
        return solutionCost / this.mult;
    }

    public void integerArcCosts(final glp_graph graph)
    {
        for (int i = 1; i < graph.getNv(); i++)
        {
            final glp_vertex v = GLPK.glp_java_vertex_get(graph, i);
            for (glp_arc a = v.getOut(); a != null; a = a.getT_next())
            {
                final glp_java_arc_data adata = GLPK.glp_java_arc_get_data(a);
                final double cost = adata.getCost();
                this.setArcCostInterging(a, Math.round(cost));
            }
        }
    }
    
    public void setArcCostInterging(final glp_arc arc, final double cost)
    {
        glp_java_arc_data adata;
        adata = GLPK.glp_java_arc_get_data(arc);
        adata.setCost(Math.round(cost * this.mult));
    }

    public void printSolution(final glp_graph graph)
    {
    	for (int i = 1; i < graph.getNv(); i++)
		{
            final glp_vertex v = GLPK.glp_java_vertex_get(graph, i);
	            for (glp_arc a = v.getOut(); a != null; a = a.getT_next()) {
	                final glp_vertex w = a.getHead();
	                final double flow = GLPK.glp_java_arc_get_data(a).getX();
	                System.out.printf("arc %d->%d: x = %2g; rc = %2g; ct = %2g; cap = %2g\n",
	                v.getI(), w.getI(),
	                    flow,
	                GLPK.glp_java_arc_get_data(a).getRc()/this.mult,
	                GLPK.glp_java_arc_get_data(a).getCost()/this.mult,
	                GLPK.glp_java_arc_get_data(a).getCap());
	            }
		}
    }
}
