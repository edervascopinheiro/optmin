package outros;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

@SuppressWarnings({ "boxing", "javadoc" })
public class ProgTest
{

	/**
	* PROBLEMA 1
	*
	* Para um dado n�mero inteiro n > 1, o menor inteiro d > 1 que divide n � chamado de
	* fator primo. � poss�vel determinar a fatora��o prima de n achando-se o fator primo d
	* e substituindo n pelo quociente n / d, repetindo essa opera��o at� que n seja igual a 1.
	* Cada fator deve receber um peso, que segue a seguinte regra alternada:
	* Por exemplo, se existirem sete fatores, O �ltimo recebe 1, o pen�ltimo 7, o pr�ximo 2, o
	* pr�ximo 6 e assim por diante.
	* Fa�a um programa que retorne a m�dia ponderada desses fatores.
	* n=3960 = 11 * 5 * 3 * 3 * 2 * 2 * 2.
	* pesos  =  1   7   2   6   3   5   4
	*/


	/**
	* Calcula a media ponderada dos fatores primos de um numero.
	* @param n Numero a ser fatorado.
	* @return retorna a m�dia ponderada.
	*/
	public static double mediaPonderadaFatoresAlternada(final int n)
	{
        final int[] vetorFatoresPrimos = ProgTest.getFatoresPrimosOrdemInversa(n);
        final int[] vetorDePesosAlternandos = ProgTest.pesosAlternadosDosFatores(vetorFatoresPrimos);
        final double somaPonderadaFatores = ProgTest.somaDosProdutosDosElementosDoVetor(vetorDePesosAlternandos.length,
            vetorDePesosAlternandos, vetorFatoresPrimos);
        final double somaDosPesosAlternados = ProgTest.somaDosElementosDoVetor(vetorDePesosAlternandos);
        return somaPonderadaFatores / somaDosPesosAlternados;
	}

    private static int somaDosProdutosDosElementosDoVetor(final int tamanhoDoVetor, final int[] vetor1, final int[] vetor2)
    {
        int soma = 0;
        for (int i = 0; i < tamanhoDoVetor; i++)
            soma += vetor1[i] * vetor2[i];
        return soma;
    }

    private static int somaDosElementosDoVetor(final int[] vetor)
    {
        int soma = 0;
        for (final int elemento : vetor)
            soma += elemento;
        return soma;
    }

    private static int[] pesosAlternadosDosFatores(final int[] vetorFatoresPrimos)
	{
	    final int[] pesos = new int[vetorFatoresPrimos.length];
        for (int i = 0; i < pesos.length; i++)
        {
            if (i > 1)
                pesos[i] = pesos[i - 2] + (i % 2 == 0 ? 1 : -1);
            if (i == 0)
                pesos[i] = 1;
            else if (i == 1)
                pesos[i] = pesos.length;
        }
	    return  pesos;
	}

    private static int[] getFatoresPrimosOrdemInversa(final int n)
    {
        final List<Integer> fatoresPrimos = new ArrayList<Integer>(n);
        int N = n;
        while (N != 1)
        {
            final int menorFatorPrimo = ProgTest.menorFatorPrimo(N);
            N = N / menorFatorPrimo;
            fatoresPrimos.add(menorFatorPrimo);
        }
        final int[] vetorFatoresPrimos = new int[fatoresPrimos.size()];
        final int tamanhoDoVetor = fatoresPrimos.size();
        for (int i = 0; i < tamanhoDoVetor; i++)
            vetorFatoresPrimos[tamanhoDoVetor - (i + 1)] = fatoresPrimos.get(i).intValue();
        return vetorFatoresPrimos;
    }

    private static int menorFatorPrimo(final int n)
    {
        for (int d = 2; d < n; d++)
            if (n % d == 0)
                return d;
        return n;
    }


	/***
    
    PROBLEMA 2
    
    Dado um vetor de inteiros positivos, verificar se existe algum valor presente
    em mais da metade (maioria absoluta) dos elementos do vetor.
     * caso exista, retorne a soma de todos os n�meros menores que o valor encontrado.
     * Caso nao exista, retornar -1.
    
    Exemplos:
    1. [] -> -1
    2. [13] -> 0
    3. [4, 7] -> -1
    5. [5, 1, 5] -> 1
    6. [2, 3, 1] -> -1
    7. [2, 1, 7, 2, 2, 8, 2] -> 1
    8. [2, 1, 7, 2, 2, 8, 2, 1] -> -1
    9. [2, 1, 7, 2, 2, 8, 2, 1, 2] -> 2
    
    ***/
    public static int problema2(final int[] inteiros)
	{

        if (inteiros.length == 0)
            return -1;
        //        final int[] inteiroQuantidade = ProgTest.quantidadesDeCadaInteriro(inteiros);
        //
        //        final int indiceDoInteiroComMaiorQuantidade = ProgTest.indiceDoInteriroComMaiorPresen�a(inteiroQuantidade);
        //
        //        if (inteiroQuantidade[indiceDoInteiroComMaiorQuantidade] > (inteiroQuantidade.length / 2))
        //            return ProgTest.obtemSomaDosMenores(inteiros[indiceDoInteiroComMaiorQuantidade], inteiros);

        Arrays.sort(inteiros);
        int valorModa = inteiros[0];
        int qtdModa = 1;
        int count = 1;
        for (int index = 0; index < inteiros.length; index++)
        {
            if (index == inteiros.length - 1 || inteiros[index] != inteiros[index + 1])
            {
                if (count > qtdModa)
                {
                    qtdModa = count;
                    valorModa = inteiros[index - 1];
                }
                count = 1;
            }
            else
                count++;
        }
        if (qtdModa * 2 > inteiros.length)
            return ProgTest.obtemSomaDosMenores(valorModa, inteiros);
        return -1;
	}

    private static int indiceDoInteriroComMaiorPresen�a(final int[] inteiroQuantidade)
    {
        int maior = 0;
        int indexMaior = 0;
        for (int i = 0; i < inteiroQuantidade.length; i++)
        {
            if (inteiroQuantidade[i] > maior)
            {
                maior = inteiroQuantidade[i];
                indexMaior = i;
            }
        }
        return indexMaior;
    }

    private static int obtemSomaDosMenores(final int sup, final int[] inteiros)
    {
        int soma = 0;
        for (final int inteiro : inteiros)
            if (inteiro < sup)
                soma += inteiro;
        return soma;
    }

    private static int[] quantidadesDeCadaInteriro(final int[] inteiros)
    {
        final int[] inteiroQuantidade = new int[inteiros.length];
        int elemento;
        for (int i = 0; i < inteiros.length; i++)
        {
            elemento = inteiros[i];
            for (int j = 0; j < inteiros.length; j++)
            {
                if (elemento == inteiros[j])
                    inteiroQuantidade[i] += 1;
            }
        }
        return inteiroQuantidade;
    }

	/**
	* PROBLEMA 3
	* calculadora polonesa
	* Valor de Express�o Polonesa.   Suponha que posf � uma string que guarda uma
	* express�o aritm�tica em nota��o posfixa. Suponha que posf n�o � vazio e cont�m
	* somente os operadores  +,  -,  *  e  /  (todos exigem dois operandos).
	* Escreva um algoritmo que receba posf e calcule o valor da express�o.
	* mais detalhes sobre o m�todo polones inverso: http://pt.wikipedia.org/wiki/Nota%C3%A7%C3%A3o_polonesa_inversa
	*/


	/**
    * Calcula o valor de uma express�o em nota��o polonesa.
    * @param entrada String com uma express�o (ex.: 10 5 +)
    * @return Retorna o valor da express�o polonesa
    */
    public static double polonesa(final String entrada)
	{
        final String[] entradaArray = ProgTest.parceitaEntrada(entrada);
        final Stack<Double> pilha = new Stack<Double>();
        for (final String caracter : entradaArray)
        {
            if (ProgTest.isOperador(caracter))
                pilha.push(ProgTest.executaOperacao(caracter, pilha.pop(), pilha.pop()));
            else
                pilha.push(Double.parseDouble(caracter));
        }
        return pilha.pop().doubleValue();
	}

    private static boolean isOperador(final String caracter)
    {
        if (!caracter.matches("\\d+"))
            return true;
        return false;
    }

    private static double executaOperacao(final String operador, final double v2, final double v1)
    {
        switch (operador)
        {
        case "+":
            return v1 + v2;
        case "-":
            return v1 - v2;
        case "*":
            return v1 * v2;
        case "/":
            return v1 / v2;
        default:
            return Double.NaN;
        }
    }

    private static String[] parceitaEntrada(final String entrada)
    {
        final List<String> listaEntradaSemEspaco = new ArrayList<String>(entrada.length());
        String string = "";
        String substring;
        for (int i = 0; i < entrada.length(); i++)
        {
            substring = entrada.substring(i, i + 1);
            if (!substring.equals(" "))
                string += substring;
            if (ProgTest.isOperador(substring))
            {
                if (!string.equals(""))
                    listaEntradaSemEspaco.add(string);
                string = "";
            }
        }
        final String[] vetorEntradaSemEspaco = new String[listaEntradaSemEspaco.size()];
        for (int i = 0; i < listaEntradaSemEspaco.size(); i++)
            vetorEntradaSemEspaco[i] = listaEntradaSemEspaco.get(i);
        return vetorEntradaSemEspaco;
    }

    // one class needs to have a main() method
    public static void main(final String[] args)
    {
        System.out.println("Hello!");

        /*final int[] fatoresPrimosTest = ProgTest.getFatoresPrimosOrdemInversa(10);
        for (int i = 0; i < fatoresPrimosTest.length; i++)
            System.out.println(fatoresPrimosTest[i]);*/
        /*final double result = ProgTest.mediaPonderadaFatoresAlternada(3960);
        
        System.out.println(result);*/
        //        System.out.println(ProgTest.problema2(new int[] {}));
        //        System.out.println(ProgTest.problema2(new int[] { 13 }));
        //        System.out.println(ProgTest.problema2(new int[] { 4, 7 }));
        //        System.out.println(ProgTest.problema2(new int[] { 5, 1, 5 }));
        //        System.out.println(ProgTest.problema2(new int[] { 2, 3, 1 }));
        //        System.out.println(ProgTest.problema2(new int[] { 2, 1, 7, 2, 2, 8, 2 }));
        //        System.out.println(ProgTest.problema2(new int[] { 2, 1, 7, 2, 2, 8, 2, 1 }));
        //        System.out.println(ProgTest.problema2(new int[] { 2, 1, 7, 2, 2, 8, 2, 1, 2 }));

        System.out.println(ProgTest.polonesa("10 5 +"));
        
        System.out.println(ProgTest.polonesa("10 5 + 1 2 + - 3 2 + /"));

        System.out.println(ProgTest.polonesa("5 1 2 + 4 * + 3 -1"));

    }
}
