package outros;

import java.util.Arrays;
import java.util.Collections;

public class ForLoops
{

    public static void main(final String[] args)
    {
        /*final Scanner in = new Scanner(System.in);
        final int s = in.nextInt();
        final int n = in.nextInt();
        final int m = in.nextInt();
        final Integer[] keyboards = new Integer[n];
        for (int keyboards_i = 0; keyboards_i < n; keyboards_i++)
        {
            keyboards[keyboards_i] = in.nextInt();
        }
        final int[] pendrives = new int[m];
        for (int pendrives_i = 0; pendrives_i < m; pendrives_i++)
        {
            pendrives[pendrives_i] = in.nextInt();
        }*/

        final int s = 10;
        final Integer[] keyboards = new Integer[] { Integer.valueOf(3), Integer.valueOf(1) };
        final int[] pendrives = new int[] { 2, 5, 8 };
        Arrays.sort(keyboards, Collections.reverseOrder());//Descending order
        Arrays.sort(pendrives);//Ascending order

        int max = -1;
        int steps = 0;
        for (int i = 0, j = 0; i < keyboards.length; i++)
        {

            for (; j < pendrives.length; j++)
            {
                steps++;
                if (keyboards[i].intValue() + pendrives[j] > s)
                    break; //This prevents j from incrementing
                if (keyboards[i].intValue() + pendrives[j] > max)
                    max = keyboards[i].intValue() + pendrives[j];
            }
        }
        System.out.println(max);
        System.out.println(steps);
    }

}
