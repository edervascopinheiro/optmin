package simplex;

public interface LinearProgram {
	public void addVar(final int variableIndex, final LinearProgramVariable variable);
	
	public void addConstraint(final int constraintIndex, final LinearProgramConstraint constraint);
	
	public LinearProgramVariable[] execute();
}
