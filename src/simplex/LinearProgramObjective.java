package simplex;

public enum LinearProgramObjective
{
	MAXIMIZE,
	
	MINIMIZE;
}
