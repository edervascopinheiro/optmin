package simplex;

import matrix.LinearEquationSolving;
import matrix.MatrixOp;

public class Simplex implements LinearProgram
{

	//private final LinearProgramVariable variables[];
	
	private final double[] variables;
	
	//private final LinearProgramConstraint constraints[];
	
	private final LinearProgramObjective objective;
	
	private final double[][] A;
	
	private final double[] b;
	
	private final double[] c;
	
	private int[] basicVariableIndex;
	
	private int[] nonBasicVariableIndex;
	
	private double[][] B;
	
	private double[][] N;

	private double[] reducedCost;
	
	private double radio;//TODO not here
	
	
	/*public Simplex(final LinearProgramObjective objective, final int varNumber, final int constraintNumber, final int[] firstBasicVariableIndexes) {
		this.objective = objective;
		this.variables = new LinearProgramVariable[varNumber];
		this.constraints = new LinearProgramConstraint[constraintNumber];
		this.basicVariableIndex = firstBasicVariableIndexes;
	}
	
	public Simplex(final LinearProgramObjective objective, final int varNumber, final int constraintNumber) {
		this.objective = objective;
		this.variables = new LinearProgramVariable[varNumber];
		this.constraints = new LinearProgramConstraint[constraintNumber];
		this.basicVariableIndex = null;
	}*/


	
	public Simplex(double[] c, double[][] A, double[] b, LinearProgramObjective objective, int[] basicVariableIndex) {
		//this.variables = new LinearProgramVariable[A[0].length];
		this.variables = new double[A[0].length];
		this.c = c;
		this.A = A;
		this.b = b;
		this.objective = objective;
		this.basicVariableIndex = basicVariableIndex;
	}

	public void addConstraint(final int constraintIndex, final LinearProgramConstraint constraint) {
		//TODO exception if this index is already instantiated
		//this.constraints[constraintIndex] = constraint;
	}
	
	@Override
	public LinearProgramVariable[] execute() {
		//this.standardForm();
		//this.setCollumns();
		/*if (this.basicVariableIndex == null)
			this.basicVariableIndex = this.firstBasicFeasibleVariable();
		this.nonBasicVariableIndex = this.setNonBasicVariableIndex();
		boolean isOptimal = false;
		while (!isOptimal)
			isOptimal = executeOneStep();*/
		return null;
		
	}
	
	public void executeSteps() {
		if (this.basicVariableIndex == null)
			this.basicVariableIndex = this.firstBasicFeasibleVariable();
		this.nonBasicVariableIndex = this.setNonBasicVariableIndex();
		boolean isOptimal = false;
		while (!isOptimal)
			isOptimal = executeOneStep();
		
	}
	
	private boolean executeOneStep() {
		this.B = MatrixOp.getMatrixFromCollumns(this.A, this.basicVariableIndex);
		this.N = MatrixOp.getMatrixFromCollumns(A, this.nonBasicVariableIndex);
		this.reducedCost = this.calculateReducedCost();
		int indexInputVariable = this.selectIndexForInputVariable();
		double[] bb = LinearEquationSolving.solveEquation(this.B, this.b);
		this.setSolution(bb);
		if(indexInputVariable == -1)
			return true;
		int indexOutputVariable = this.selectOutputVariable(indexInputVariable, bb);
		this.swapVariables(indexInputVariable, indexOutputVariable);
		return false;	
	}
	
	private void swapVariables(int indexInputVariable, int indexOutputVariable) {
		int variableOut = this.basicVariableIndex[indexOutputVariable];
		int variableIn = this.nonBasicVariableIndex[indexInputVariable];
/*		this.x[variableOut] = 0;
		this.x[variableIn] = this.x[variableIn] + this.radio;
*/		this.basicVariableIndex[indexOutputVariable] = variableIn;
		this.nonBasicVariableIndex[indexInputVariable] = variableOut;
	}

	private int selectIndexForInputVariable() {
		int index = -1;
		double max = 0;		
		for(int i = 0; i < this.reducedCost.length; i++) {
			if(this.reducedCost[i] > max){
				max = this.reducedCost[i];
				index = i;
			}
		}
		return index;
	}

	private int selectOutputVariable(int indexInputVariable, double[] bb) {
		double[] Nj = MatrixOp.getCollumn(this.N, indexInputVariable);
		double[] BNj = LinearEquationSolving.solveEquation(this.B, Nj);
		double min = Double.MAX_VALUE;
		double quocient;
		int indexOutputVariable = -1;//TODO improve it
		for(int k = 0; k < bb.length; k++) {
			if(BNj[k] > 0) {
				quocient = bb[k]/BNj[k];
				if(min > quocient) {
					min = quocient;
					indexOutputVariable = k;
				}
			}
		}
		this.radio = min;
		return indexOutputVariable;
	}


	private void setSolution(double[] bb) {
		for(int i = 0; i < bb.length; i++)
			this.variables[this.basicVariableIndex[i]] = bb[i];
		for(int i = 0; i < this.nonBasicVariableIndex.length; i++)
			this.variables[this.nonBasicVariableIndex[i]] = 0;
	}

	private double[] calculateReducedCost() {
		double[][] transposeB = MatrixOp.transposeMatrix(this.B);
		double[] cb = MatrixOp.getVetorFromIndexes(this.c, this.basicVariableIndex);
		double[] cn = MatrixOp.getVetorFromIndexes(this.c, this.nonBasicVariableIndex);
		double[] y = LinearEquationSolving.solveEquation(transposeB, cb);
		double[] yN = MatrixOp.matrixVectorMultiplication(MatrixOp.transposeMatrix(this.N), y);
		double[] reducedCost = MatrixOp.subtractVectors(cn, yN);
		return reducedCost;
	}

	private int[] setNonBasicVariableIndex() {
		int[] index = new int[this.A[0].length-this.basicVariableIndex.length];
		int i = 0;
		for(int j = 0; j < A[0].length; j++) {
			int k = 0; boolean notBasicVar = true;
			while(k < this.basicVariableIndex.length && notBasicVar) {
				if(j == this.basicVariableIndex[k])
					notBasicVar = false;
				k++;
			}
			if(notBasicVar)
				index[i] = j;
			i++;
		}
		return index;
	}

	private int[] firstBasicFeasibleVariable() {
		return null;
	}

	private void standardForm() {
		this.creatingSlackVariables();
	}
	
	private void creatingSlackVariables() {
		/*for (LinearProgramConstraint con : this.constraints) {
			switch (con.geConstraintType()) {
			case GREAT_EQUAL: 
				this.variables[this.variables.length-1] 
						= new LinearProgramVariable("slackVar", 0, Double.MAX_VALUE);
				//con.addCollumn(-1);
			case LESS_EQUAL : 
				this.variables[this.variables.length-1] 
						= new LinearProgramVariable("slackVar", 0, Double.MAX_VALUE);
				//con.addCollumn(1);	
				break;
			default:
				break;
			}
		}*/
	}
	
	public void addVar(final int variableIndex, final LinearProgramVariable variable) {
		//TODO exception if this index is already instantiated
		//this.variables[variableIndex] = variable;
	}
	
	public double[] getX() {
		return variables;
	}
}
