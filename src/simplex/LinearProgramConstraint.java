package simplex;

import java.util.List;

public class LinearProgramConstraint
{
    private final String constraintName;

    private final double[] a;
    
    private final double b;

    private final ConstraintType constraintType;

	public LinearProgramConstraint(String constraintName, double[] a, double b, ConstraintType constraintType) {
		this.constraintName = constraintName;
		this.a = a;
		this.b = b;
		this.constraintType = constraintType;
	}

	public ConstraintType geConstraintType() {
		return this.constraintType;
	}	

}
