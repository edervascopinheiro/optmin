package simplex;

public enum ConstraintType
{
    IGUALITY
    {
        @Override
        public boolean apply(final double a, final double b)
        {
            return a == b;
        }
    },

    LESS_EQUAL
    {
        @Override
        public boolean apply(final double a, final double b)
        {
            return a <= b;
        }
    },

    GREAT_EQUAL
    {
        @Override
        public boolean apply(final double a, final double b)
        {
            return a >= b;
        }
    };

    String sign;

    public abstract boolean apply(double a, double b);
}
