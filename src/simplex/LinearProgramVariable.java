package simplex;

public class LinearProgramVariable
{
	private final String name;
	
    private final double upperBound;

    private final double lowerBound;
    
    private double value;

	public LinearProgramVariable(String name, double upperBound, double lowerBound) {
		this.name = name;
		this.upperBound = upperBound;
		this.lowerBound = lowerBound;
	}

	public double getValue() {
		return value;
	}
}
